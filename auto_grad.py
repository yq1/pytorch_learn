
# blog: https://blog.paperspace.com/pytorch-101-understanding-graphs-and-automatic-differentiation/
# tutorial: https://pytorch.org/tutorials/beginner/blitz/autograd_tutorial.html#sphx-glr-beginner-blitz-autograd-tutorial-py
import torch
# this script teach us how to calculate gradient:

# if z is a function of x, we want to know the gradiant of z|x

#example 1: out is a scalar
x = torch.ones(2, 2, requires_grad=True)
print(x)

y = x + 2
print(y)

z = y * y * 3
out = z.mean()

print(z, out)

out.backward(torch.tensor([1.0,2.0,3.0])) # default value: torch.tensor(1.0)
#Print gradients d(out)/dx

print(x.grad)

#example 2:
# out is not a scalar
x = torch.randn(3, requires_grad=True)

y = x * 2
while y.data.norm() < 1000:
    y = y * 2

print(y)

v = torch.tensor([0.1, 1.0, 0.0001], dtype=torch.float)
y.backward(v)

print(x.grad)
